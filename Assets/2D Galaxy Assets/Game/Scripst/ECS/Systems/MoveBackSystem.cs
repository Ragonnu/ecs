﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;


public class MoveBackSystem : SystemBase
{
    //barrera. Com el postUpdate. Et permet fer coses thread-safe com afegir o esborrar entitats paralelament.
    EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

    protected override void OnUpdate()
    {
        var commandBuffer = barrier.CreateCommandBuffer().ToConcurrent();

        float dt = Time.DeltaTime;
        Entities.WithAny<EnemyTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation trans, in MoveBackComponent mBack, in MoveForward moveForward) =>
        {
            if(trans.Value.y < -6f)
            {
                //Debug.Log(entityInQueryIndex);
                trans.Value = mBack.position;
            }
        }
        ).ScheduleParallel();
    }

}
