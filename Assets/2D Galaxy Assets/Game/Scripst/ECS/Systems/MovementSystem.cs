﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;


public class MovementSystem : SystemBase
{
    //barrera. Com el postUpdate. Et permet fer coses thread-safe com afegir o esborrar entitats paralelament.
    EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

    protected override void OnUpdate()
    {
        var commandBuffer = barrier.CreateCommandBuffer().ToConcurrent();

        float dt = Time.DeltaTime;

        Entities.WithAny<EnemyTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation trans, in Rotation rot, in MoveForward moveForward) =>
        {
            //Debug.Log(entityInQueryIndex);
            trans.Value += moveForward.speed * dt * math.mul(rot.Value, new float3(0, -1, 0));
        }
        ).ScheduleParallel();

        Entities.WithAny<LaserTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation trans, in Rotation rot, in MoveForward moveForward) =>
        {
            //Debug.Log(entityInQueryIndex);
            trans.Value += moveForward.speed * dt * math.mul(rot.Value, new float3(0, 1, 0));
        }
        ).ScheduleParallel();
    }

}