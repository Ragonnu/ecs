﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class MoricionSystem : ComponentSystem
{

    private float collisionDistance = 0.5f;
    EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

    protected override void OnUpdate()
    {
        var commandBuffer = barrier.CreateCommandBuffer().ToConcurrent();

        Entities.WithAll<EnemyTag>().ForEach((Entity entity, ref Translation enemyPos) =>
        {
            float3 playerPos = (float3)GameManager.GetPlayerPosition();

            //Debug.Log(enemyPos.Value);
            //Debug.Log(math.distance(playerPos, enemyPos.Value));
            //playerPos.z = enemyPos.Value.z;
            if(math.distance(playerPos, enemyPos.Value) < 2)
            {
                //PostUpdateCommands.DestroyEntity(entity);
            }

            float3 enemyPosition = enemyPos.Value;

            Entities.WithAny<LaserTag>().ForEach((Entity laser, ref Translation bulletPos) =>
            {
                if (math.distance(bulletPos.Value, enemyPosition) < collisionDistance)
                {
                    PostUpdateCommands.DestroyEntity(entity);
                    PostUpdateCommands.DestroyEntity(laser);
                }
            });
        });
    }
}
