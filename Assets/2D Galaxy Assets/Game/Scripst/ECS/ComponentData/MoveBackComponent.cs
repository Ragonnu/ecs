﻿using Unity.Entities;
using UnityEngine;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

[GenerateAuthoringComponent]
public struct MoveBackComponent : IComponentData
{
    public Vector3 position;
}
