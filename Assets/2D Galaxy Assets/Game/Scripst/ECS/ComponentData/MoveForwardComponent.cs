﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

[GenerateAuthoringComponent]
public struct MoveForward : IComponentData
{
    public float speed;

}
