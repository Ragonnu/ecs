﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public bool canTripleShoot;
    public bool canSpeedBoost;
    public bool canShield;

    public int live = 3;


    private float _canFire = 0.0f;
    [SerializeField]
    private float _fireRate = 0.01f;
    [SerializeField]
    private GameObject _tripleLaser;
    [SerializeField]
    private GameObject _laserPrefab;
    [SerializeField]
    private GameObject _Explosion;
    [SerializeField]
    private GameObject _playerShield;
    [SerializeField]
    private GameObject[] _engines;
    [SerializeField]//Con esto, hacemos que una variable privada se pueda ver en el inspector de Unity.
    private float _speed = 3.0f;

    private UiManager _uiManager;
    private GameManager _gameManager;
    private AudioSource _audioSource;
    private int _hitCount = 0;


    // Start is called before the first frame update
    void Start()
    {
        _hitCount = 0;
        _uiManager = GameObject.Find("Canvas").GetComponent<UiManager>();
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _audioSource = GetComponent<AudioSource>();

        if (_uiManager != null)
        {
            _uiManager.UpdateLives(live);
        }
    }

    // Update is called once per frame
    void Update()
    {
        movement();
        if(Input.GetMouseButton(0) && Time.time > _canFire)
        {
            shoot();
        }
    }
    
    private void shoot()
    {
        _audioSource.Play();
        if (canTripleShoot)
        {
            Instantiate(_tripleLaser, transform.position + new Vector3(0, 0, 0), Quaternion.identity);
        }
        else
        {
            //Instantiate(_laserPrefab, transform.position + new Vector3(0, 0.8f, 0), Quaternion.identity);
            EnemySpawner.Instance.SpawnLaser(transform.position + new Vector3(0, 0.8f, 0));
        }
        _canFire = Time.time + _fireRate;
    }

    private void movement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        if (canSpeedBoost)
        {
            transform.Translate(Vector3.right * _speed * 2.0f * horizontalInput * Time.deltaTime);
            transform.Translate(Vector3.up * _speed * 2.0f * verticalInput * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.right * _speed * horizontalInput * Time.deltaTime);
            transform.Translate(Vector3.up * _speed * verticalInput * Time.deltaTime);
        }

        if (transform.position.y > 4)
        {
            transform.position = new Vector3(transform.position.x, 4f, 0);
        }
        if (transform.position.y < -4)
        {
            transform.position = new Vector3(transform.position.x, -4f, 0);
        }
        if (transform.position.x > 8)
        {
            transform.position = new Vector3(8f, transform.position.y, 0);
        }
        if (transform.position.x < -8)
        {
            transform.position = new Vector3(-8f, transform.position.y, 0);
        }
    }

    public void Damage()
    {
        if (canShield)
        {
            canShield = false;
            _playerShield.SetActive(false);
        }
        else
        {
            live--;
            _hitCount++;
            if (_hitCount == 1)
            {
                _engines[0].SetActive(true);
            }
            else if (_hitCount == 2)
            {
                _engines[1].SetActive(true);
                _engines[2].SetActive(true);

            }
            _uiManager.UpdateLives(live);
            if (live <= 0)
            {
                
                Instantiate(_Explosion, transform.position, Quaternion.identity);
                _gameManager.gameOver = true;
                _uiManager.ShowMainMenu();
                live = 3;
                Destroy(this.gameObject);
            }
        }
        
    }
    public void PowerUp1On()
    {
        canTripleShoot = true;
        StartCoroutine(PowerUp1Routine());
    }

    public void PowerUp2On()
    {
        canSpeedBoost = true;
        _fireRate = 0.0f;
        StartCoroutine(PowerUp2Routine());
    }

    public void powerUp3On()
    {
        canShield = true;
        _playerShield.SetActive(true);
    }

    public IEnumerator PowerUp1Routine()
    {
        yield return new WaitForSeconds(5.0f);
        canTripleShoot = false;
    }

    public IEnumerator PowerUp2Routine()
    {
        yield return new WaitForSeconds(5.0f);
        canSpeedBoost = false;
        _fireRate = 0.25f;
    }

}
