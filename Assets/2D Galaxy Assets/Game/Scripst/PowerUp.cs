﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField]
    private float _speed = 5f;
    [SerializeField]
    private int powerUpId;
    [SerializeField]
    private AudioClip _clip;


    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);
        if (transform.position.y < -6)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player(Clone)")
        {
            Player player = other.GetComponent<Player>();

            if (player != null)
            {
                AudioSource.PlayClipAtPoint(_clip, Camera.main.transform.position);
                if (powerUpId == 0)
                {
                    player.PowerUp1On();
                }
                else if (powerUpId == 1)
                {
                    player.PowerUp2On();
                }
                else if (powerUpId == 2)
                {
                    player.powerUp3On()
;                }
                
            }

            Destroy(this.gameObject);
        }
    }
        
}
