﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [SerializeField]
    private float _speed = 5.0f;
    
    [SerializeField]
    private GameObject _enemyExplosion;
    private UiManager _uiManager;
    [SerializeField]
    private AudioClip _clip;

    void Start()
    {
        _uiManager = GameObject.Find("Canvas").GetComponent<UiManager>();
    }

    void Update()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);
        if (transform.position.y < -6)
        {
            transform.position = new Vector3(Random.Range(-7.0f, 7.0f), 8f, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Player player = other.GetComponent<Player>();
            player.Damage();
            Instantiate(_enemyExplosion, transform.position, Quaternion.identity);
            //aDestroy(_enemyExplosion.gameObject);
            //_uiManager.UpdateScore();
            AudioSource.PlayClipAtPoint(_clip, Camera.main.transform.position);
            Destroy(this.gameObject);
        }
        else if (other.CompareTag("Laser"))
        {
            Debug.Log("laser");
            Destroy(other.gameObject);
            Instantiate(_enemyExplosion, transform.position, Quaternion.identity);
            //Destroy(_enemyExplosion.gameObject);
            _uiManager.UpdateScore();
            AudioSource.PlayClipAtPoint(_clip, Camera.main.transform.position);
            Destroy(this.gameObject);
        }
        
        

    }
}
