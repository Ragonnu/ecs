﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public Sprite[] lives;
    public Image livesImage;
    public Text score;
    [SerializeField]
    private GameObject _mainMenu;
    public int points = 100;

    public void UpdateLives(int currentLives)
    {
        livesImage.sprite = lives[currentLives];
    }
    public void ResetScore()
    {
        points = 0;
        score.text = "Score: " + points;
    }
    public void UpdateScore()
    {
        points += 10;
        score.text = "Score: " + points;
    }
    public void ShowMainMenu()
    {
        //points = 0;
        //score.text = "Score: " + points;
        _mainMenu.SetActive(true);
    }
    public void HideMainMenu()
    {
        _mainMenu.SetActive(false);
    }

}
