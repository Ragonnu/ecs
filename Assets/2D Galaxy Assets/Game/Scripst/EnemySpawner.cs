﻿using UnityEngine;
using Unity.Mathematics;
using Unity.Entities;


using Random = UnityEngine.Random;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Collections;


public class EnemySpawner : MonoBehaviour
{
    public static EnemySpawner Instance;

    [Header("Spawner")]
    // number of enemies generated per interval
    [SerializeField] private int spawnCount = 1;

    // time between spawns
    [SerializeField] private float spawnInterval = 0.001f;

    [Header("Enemy")]
    // random speed range
    [SerializeField] float minSpeed = 4f;
    [SerializeField] float maxSpeed = 40f;

    // counter
    private float spawnTimer;

    // flag from GameManager to enable spawning
    private bool canSpawn;


    //entityManager
    private EntityManager entityManager;

    [SerializeField]
    private GameObject laserPrefab;
    private Entity entityPrefabLaser;

    [SerializeField]
    private GameObject enemyPrefab;
    private Entity entityPrefab;

    // simple Singleton
    protected virtual void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    private void Start()
    {
        //DefaultGameObjectInjectionWorld es el metode clau del ECS hibrid. Permet convertir GameObjects en Entitats
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        entityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(enemyPrefab, settings);
        entityPrefabLaser = GameObjectConversionUtility.ConvertGameObjectHierarchy(laserPrefab, settings);

        if (!GameManager.Instance.gameOver)
        {
            SpawnWave();
        }
    }

    // spawns enemies in a ring around the player
    private void SpawnWave()
    {
        transform.position = new Vector3(Random.Range(-7.0f, 6.0f), 8f, 0);

        NativeArray<Entity> enemyArray = new NativeArray<Entity>(spawnCount, Allocator.Temp);
        for (int i =0;i<enemyArray.Length; i++)
        {
            enemyArray[i] = entityManager.Instantiate(entityPrefab);
            entityManager.SetComponentData(enemyArray[i], new Translation { Value = transform.position});
            entityManager.SetComponentData(enemyArray[i], new Rotation { Value = Quaternion.Euler(Vector2.right)});
            entityManager.SetComponentData(enemyArray[i], new MoveForward { speed = Random.Range(minSpeed, maxSpeed)});
            entityManager.SetComponentData(enemyArray[i], new MoveBackComponent { position = transform.position});
        }

        //com la llista es temporal se li hauria de fer un Dispose
        enemyArray.Dispose();
    }

    public void SpawnLaser(Vector3 pos)
    {
        transform.position = new Vector3(Random.Range(-7.0f, 7.0f), 8f, 0);

        Entity laser = entityManager.Instantiate(entityPrefabLaser);
        entityManager.SetComponentData(laser, new Translation { Value = pos});
        entityManager.SetComponentData(laser, new Rotation { Value = Quaternion.Euler(Vector2.right)});
        entityManager.SetComponentData(laser, new MoveForward { speed = 8});
        //entityManager.SetComponentData(laser, new MoveBackComponent { position = transform.position });
    }

    // signal from GameManager to begin spawning
    public void StartSpawn()
    {
        canSpawn = true;
    }

    private void Update()
    {
        // disable if the game has just started or if player is dead
        //if (!canSpawn || GameManager.IsGameOver())
        //{
        //    return;
        //}

        // count up until next spawn
        spawnTimer += Time.deltaTime;

        // spawn and reset timer
        if (spawnTimer > spawnInterval && !GameManager.Instance.gameOver)
        {
            SpawnWave();
            spawnTimer = 0;
        }
    }
}
