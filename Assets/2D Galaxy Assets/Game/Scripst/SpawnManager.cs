﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _enemyShip;
    [SerializeField]
    private GameObject[] _powerUps;
    [SerializeField]
    private GameObject _player;
    private GameManager _gameManager;

    // Start is called before the first frame update
    void Start()
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        
    }
    public void StartSpawn()
    {
        StartCoroutine(EnemySpawnRoutine());
        StartCoroutine(PowerUpSpawnRoutine());
        
    }
    public IEnumerator EnemySpawnRoutine()
    {
        while (!_gameManager.gameOver)
        {
            transform.position = new Vector3(Random.Range(-7.0f, 7.0f), 8f, 0);
            Instantiate(_enemyShip, transform.position, Quaternion.identity);

            if (Time.time < 10)
            {
                yield return new WaitForSeconds(5.0f);
            }
            else if (Time.time >= 10 && Time.time < 20)
            {
                yield return new WaitForSeconds(4.0f);
            }
            else if (Time.time >= 20 && Time.time < 40)
            {
                yield return new WaitForSeconds(3.0f);
            }
            else if (Time.time >= 40 && Time.time < 70)
            {
                yield return new WaitForSeconds(2.0f);
            }
            else if (Time.time >= 70 && Time.time < 80)
            {
                yield return new WaitForSeconds(1.0f);
            }
            else if (Time.time >= 80)
            {
                yield return new WaitForSeconds(0.3f);
            }
        }
    }
    public IEnumerator PowerUpSpawnRoutine()
    {
        while (!_gameManager.gameOver)
        {
            int r = Random.Range(0, 3);
            transform.position = new Vector3(Random.Range(-7.0f, 7.0f), 8f, 0);
            Instantiate(_powerUps[r], transform.position, Quaternion.identity);
            yield return new WaitForSeconds(18.0f);
        }
    }
}
