﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
    private Animator _animator;
    [SerializeField]
    private KeyCode _left;
    [SerializeField]
    private KeyCode _right;

    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            _animator.SetBool("TurnLeft", true);
            _animator.SetBool("TurnRight", false);
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            _animator.SetBool("TurnLeft", false);
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            _animator.SetBool("TurnRight", false);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            _animator.SetBool("TurnLeft", false);
            _animator.SetBool("TurnRight", true);
        }
    }
}
