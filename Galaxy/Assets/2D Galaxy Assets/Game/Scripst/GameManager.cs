﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public bool gameOver = true;
    [SerializeField]
    private GameObject _player;
    
    private UiManager _uiManager;
    private SpawnManager _spawnManager;

    private void Start()
    {
        _uiManager = GameObject.Find("Canvas").GetComponent<UiManager>();
        _spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
    }
    void Update()
    {
        if (gameOver)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                gameOver = false;
                _uiManager.ResetScore();
                _uiManager.HideMainMenu();
                Instantiate(_player, new Vector3(0, 0, 0), Quaternion.identity);
                _spawnManager.StartSpawn();
            }
        }
    }
}
